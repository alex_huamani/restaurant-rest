<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/reserves', 'ReserveController@index');
Route::get('/reserves/{id}', 'ReserveController@show');
Route::post('/reserves', 'ReserveController@store');
Route::put('/reserves/{id}', 'ReserveController@update');
Route::delete('/reserves/{id}', 'ReserveController@delete');
Route::get('/combos', 'ComboController@index');
Route::get('/combos/{id}', 'ComboController@show');
Route::get('/districts', 'DistrictController@index');
Route::get('/districts/{id}', 'DistrictController@show');
