<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    protected $table = 'reserve';
    protected $fillable = ['name', 'last_name', 'email', 'phone', 'reserve_day',
        'people_amount', 'combo_id', 'comment', 'district_id'];
}
