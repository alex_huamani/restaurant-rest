<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserve', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('email');
            $table->unsignedBigInteger('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('district');
            $table->string('phone');
            $table->timestamp('reserve_day')->nullable();
            $table->integer('people_amount');
            $table->unsignedBigInteger('combo_id')->unsigned();
            $table->foreign('combo_id')->references('id')->on('combo');
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserve');
    }
}
